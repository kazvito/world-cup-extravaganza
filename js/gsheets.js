{
  {
    // Your API KEY
    const API_KEY = "AIzaSyBEXMqT9egmFte8szQmpytyvpHu0V03osU";
    let sheetsData = null;
    $("#submission-name").change(() => {
      let playerName = $("#submission-name").val();
      let valueRanges = sheetsData?.valueRanges;
      for (const key in valueRanges) {
        //if dropdown value matches playername in google sheets
        if (valueRanges[key].range.includes(playerName)) {
          setPlayerDataToScreen(valueRanges[key].values);
        }
      }
    });
    //object to associate people to imgs

    function setStandings(standings) {
      const peopleImageObject = {
      Ashley: "../img/Ashley.jpg",
      Apollo: "../img/ApolloProphet.png",
      Boo: "../img/Boo.png",
      Maria: "../img/Mari.png",
      Scott: "../img/Scott.png",
      German: "../img/German.png",
      Claudia: "../img/Claudia.png",
      Juan: "../img/Juan.jpg",
      Eduardo: "../img/Eduardo.png",
      Ligia: "../img/Ligia.jpeg",
      Berta: "../img/Berta.jpg",
      Lucia: "../img/Lucia.png",
      Clara: "../img/Clara.jpeg",
    };

      let values = standings.values;
      $(".standings-table tbody").html("");
      let placing = 1;
      for (key in values) {
        let standing = values[key];
        let imageURL = peopleImageObject[standing[0]];
        if (window.location.pathname == "/world-cup-extravaganza/index.html") {
          imageURL = imageURL.substring(1);
        }
        const firstPlace = `<ion-icon name="trophy-sharp" class="trophy-gold"></ion-icon> 1
              <span class="superscript">st</span>`;
        const secondPlace = `<ion-icon name="trophy-sharp" class="trophy-silver"></ion-icon> 2
              <span class="superscript">st</span>`;
        const thirdPlace = `<ion-icon name="trophy-sharp" class="trophy-bronze"></ion-icon> 3
              <span class="superscript">st</span>`;
        if (placing == 1) {
          $(".standings-table tbody").append(
            `<tr>
            <th>
              ${firstPlace}
            </th>
            <td class="standings-cell"><img style="max-width:40px" src="${imageURL}"/>${standing[0]}</td>
            <td>${standing[1]}</td>
          </tr>`
          );
          $(".first-place .points span").text(standing[1]);
          $(".first-place-img").css(
            "background-image",
            "url(" + imageURL + ")"
          );
        } else if (placing == 2) {
          $(".standings-table tbody").append(
            `<tr>
            <th>
              ${secondPlace}
            </th>
            <td class="standings-cell"><img style="max-width:40px" src="${imageURL}"/>${standing[0]}</td>
            <td>${standing[1]}</td>
          </tr>`
          );
          $(".second-place .points span").text(standing[1]);
          $(".second-place-img").css(
            "background-image",
            "url(" + imageURL + ")"
          );
        } else if (placing == 3) {
          $(".standings-table tbody").append(
            `<tr>
            <th>
              ${thirdPlace}
            </th>
            <td class="standings-cell"><img style="max-width:40px" src="${imageURL}"/>${standing[0]}</td>
            <td>${standing[1]}</td>
          </tr>`
          );
          $(".third-place .points span").text(standing[1]);
          $(".third-place-img").css(
            "background-image",
            "url(" + imageURL + ")"
          );
        } else {
          $(".standings-table tbody").append(
            `<tr>
            <th>
              ${placing}
            </th>
            <td class="standings-cell"><img style="max-width:40px" src="${imageURL}"/>${standing[0]}</td>
            <td>${standing[1]}</td>
          </tr>`
          );
        }

        placing++;
      }
    }
    function setResults(results) {
      let values = results.values;
      let teamCounter = 1;
      for (let i = 0; i < values.length; i += 3) {
        let firstRow = values[i];
        let secondRow = values[i + 1];

        for (let j = 0; j < firstRow.length; j += 3) {
          if (typeof firstRow[j + 1] === undefined || firstRow[j + 1] == "") {
            $("#home-score-" + teamCounter).text("TBD");
          } else {
            $("#home-score-" + teamCounter).text(firstRow[j + 1]);
          }
          if (typeof secondRow[j + 1] === undefined || secondRow[j + 1] == "") {
            $("#away-score-" + teamCounter).text("TBD");
          } else {
            $("#away-score-" + teamCounter).text(secondRow[j + 1]);
          }
          teamCounter++;
        }
      }
    }

    function setPlayerDataToScreen(values) {
      let teamCounter = 1;
      for (let i = 0; i < values.length; i += 3) {
        let firstRow = values[i];
        let secondRow = values[i + 1];

        for (let j = 0; j < firstRow.length; j += 3) {
          if (typeof firstRow[j + 1] === undefined || firstRow[j + 1] == "") {
            $("#home-score-" + teamCounter).text("TBD");
          } else {
            $("#home-score-" + teamCounter).text(firstRow[j + 1]);
          }
          if (typeof secondRow[j + 1] === undefined || secondRow[j + 1] == "") {
            $("#away-score-" + teamCounter).text("TBD");
          } else {
            $("#away-score-" + teamCounter).text(secondRow[j + 1]);
          }
          teamCounter++;
        }
      }
    }

    const ranges = [
      "Ashley!B3:S25",
      "Apollo!B3:S25",
      "Scott!B3:S25",
      "German!B3:S25",
      "Claudia!B3:S25",
      "Juan!B3:S25",
      "Maria!B3:S25",
      "Eduardo!B3:S25",
      "Berta!B3:S25",
      "Ligia!B3:S25",
      "Lucia!B3:S25",
      "Clara!B3:S25",
      "Boo!B3:S25",
      "Results!B3:S25",
      "Results!F28:G40",
    ];
    function start() {
      // 2. Initialize the JavaScript client library.
      gapi.client
        .init({
          apiKey: "AIzaSyBEXMqT9egmFte8szQmpytyvpHu0V03osU",
        })
        .then(function () {
          // 3. Initialize and make the API request.
          return gapi.client.request({
            path: "https://sheets.googleapis.com/v4/spreadsheets/1C_xpkUC8-32Tpmiswn8wcjzpHFnDRx-4p1FAFGREwTk/values:batchGet",
            params: {
              ranges: [
      "Ashley!B3:S25",
      "Apollo!B3:S25",
      "Scott!B3:S25",
      "German!B3:S25",
      "Claudia!B3:S25",
      "Juan!B3:S25",
      "Maria!B3:S25",
      "Eduardo!B3:S25",
      "Berta!B3:S25",
      "Ligia!B3:S25",
      "Lucia!B3:S25",
      "Clara!B3:S25",
      "Boo!B3:S25",
      "Results!B3:S25",
      "Results!F28:G40",
    ]
            },
          });
        })
        .then(
          function (response) {
            sheetsData = response.result;
            console.log(sheetsData);
            setResults(sheetsData.valueRanges[13]);
            setStandings(sheetsData.valueRanges[14]);
          },
          function (reason) {
            //console.log("Error: " + reason.result.error.message);
            console.log(reason);
          }
        );

      // pull results
    }
    // 1. Load the JavaScript client library.
    gapi.load("client", start);
  }
}
