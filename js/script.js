/* MODAL */
let modalBtn = document.getElementById("modal-btn");
let modal = document.querySelector(".modal");
let closeBtn = document.querySelector(".close-btn");
if (modalBtn) {
  modalBtn.onclick = function () {
    modal.style.display = "block";
  };
}
if (closeBtn) {
  closeBtn.onclick = function () {
    modal.style.display = "none";
  };
}
window.onclick = function (e) {
  if (e.target == modal) {
    modal.style.display = "none";
  }
};

/////////////////////////////////////////////////////////////
//Smooth scrolling animation
const headerEl = document.querySelector(".header");

const allLinks = document.querySelectorAll("a:link");
allLinks.forEach(function (link) {
  link.addEventListener("click", function (e) {
    e.preventDefault();
    const href = link.getAttribute("href");
    // Scroll back to top;
    if (href === "#") {
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    }
    // Scroll to other links
    if (href !== "#" && href.startsWith("#")) {
      const sectionEl = document.querySelector(href);
      sectionEl.scrollIntoView({ behavior: "smooth" });
    }
    if (!href.includes("#") || href.startsWith("https://docs.google.com")) {
      if (href.startsWith("https://docs.google.com")) {
        window.open(href, "_blank");
      } else {
        window.location.href = href;
      }
    }

    //Close Mobile Navigation
    if (link.classList.contains("main-nav-link"))
      headerEl.classList.toggle("nav-open");
  });
});

/*CURRENT YEAR*/
const yearEl = document.querySelector(".year");
const currentYear = new Date().getFullYear();
yearEl.textContent = currentYear;

/* Sticky Navigation */
const sectionHeroEl = document.querySelector(".section-hero");
const obs = new IntersectionObserver(
  function (entries) {
    const ent = entries[0];
    console.log(ent);
    if (ent.isIntersecting === false) {
      document.body.classList.add("sticky");
    }
    if (ent.isIntersecting === true) {
      document.body.classList.remove("sticky");
    }
  },

  {
    //In the viewport
    root: null,
    threshold: 0,
    rootMargin: "-80px",
  }
);
if (sectionHeroEl) {
  obs.observe(sectionHeroEl);
}
document.getElementsByClassName("year")[0].innerHTML = new Date().getFullYear();
